#define _USE_MATH_DEFINES
#include <iostream>
#include <fstream>
#include <boost/math/special_functions/erf.hpp>
#include<gsl/gsl_sf_lambert.h>

using namespace std;
using namespace boost::math;

//Code that implements the Geometric Multiplex Model with Link Persistence (GMM-LP).
//The model parameters (see paper) can be tuned in lines 14, 17,18, 21-24, and 29-32.

//Link persistence probability.
double w = 0.27;

//Radial and Angular correlation strength.
double nu = 0.47;
double g = 0.82;

//Layer 1 parameters (average degree, power law exponent, temperature, number of nodes).
double bar_k_1 = 4.43;
double gamma_1 = 2.6;
double T_1 = 0.68;
double N_1 = 838;
double c_1 = bar_k_1*(sin(T_1*M_PI)/(2*T_1))*pow((gamma_1-2)/(gamma_1-1), 2);
double R_1 = 2*log(N_1/c_1);

//Layer 2 parameters (average degree, power law exponent, temperature, number of nodes).
double bar_k_2 = 2.9;
double gamma_2 = 2.8;
double T_2 = 0.65;
double N_2 = 755;
double c_2 = bar_k_2*(sin(T_2*M_PI)/(2*T_2))*pow((gamma_2-2)/(gamma_2-1), 2);
double R_2 = 2*log(N_2/c_2);

double Z, Phi1, sigma;

double Random(); 
double get_correlated_angle_with(double theta);

class Node {

        private:
        int name;
        double theta, r;
        vector<Node*> adjNodeList;

        public:
        Node(int id) {name = id;}
        
        int getName() {return name;}

        void setAngle(double angle) {theta = angle;}
        double getAngle() {return theta;}

        void setRadius(double radius) {r = radius;}
        double getRadius() {return r;}

        void addAdjNode(Node *adj) {adjNodeList.push_back(adj);}
        vector<Node*> getAdjNodeList() {return adjNodeList;}

        bool checkLink(Node *x) {
             for (int i = 0; i < adjNodeList.size(); i++) {
                  if (adjNodeList[i] == x) { return true; }
             }
             return false;
        }

};

class Graph {

        private:
        vector<Node*> nodeList;

        public:
        void addNewNode(Node *nNode) { nodeList.push_back(nNode); }

        Node* findNode(int name) {
              for (int i = 0; i < nodeList.size(); i++) {
	           if (nodeList[i]->getName() == name) return nodeList[i];
	      }
	      return NULL;
        }

        vector<Node*> getNodeList() { return nodeList; }
};

double Random() {
   double r = ((double)rand()/(double)RAND_MAX);
   return r;
}

double get_correlated_angle_with(double theta_1) {

    double l = sqrt(2)*sigma*(erf_inv(2*(Z*Random()+Phi1)-1));

    double theta_2 = fmod(theta_1+(2*M_PI/N_2)*l, 2*M_PI);
    if (theta_2 < 0) {
        theta_2 = 2*M_PI+theta_2;
    }

    return theta_2;
}

int main(int argc, char *argv[]) {

        cout.precision(15);
        srand((unsigned)time(NULL));

        ofstream out1, out2, out3, out4;
        out1.precision(15);
        out2.precision(15);
        out3.precision(15);
        out4.precision(15);

        //Output files.
        out1.open("l_1.txt");
        out2.open("l_1_coordinates.txt");
        out3.open("l_2.txt");
        out4.open("l_2_coordinates.txt");

        double Beta_1 = 1.0/(gamma_1-1);
        double Beta_2 = 1.0/(gamma_2-1);
        
        double eta=1.0/(1.0-nu);

        //For correlated angle calculations.*****
        double sigma0 = N_2/(4*M_PI);
        if(sigma0 > 100) sigma0 = 100;
        sigma = sigma0*(1.0/g-1);
        double x = N_2/(2*sigma);
        double Phi2 = 0.5*(1+erf(x/sqrt(2)));
        Phi1 = 0.5*(1+erf(-x/sqrt(2)));
        Z = Phi2 - Phi1;
        //***************************************

        Graph *G_1 = new Graph();
        Graph *G_2 = new Graph();
        Node *u;
        Node *v;

        //First sample radial and angular coordinates for nodes in L1.
        for (int i = 1; i <= N_1; i++) {

             u = new Node(i);
             G_1->addNewNode(u);

             double r = 0;
             do {
                r = R_1 + 2*Beta_1*log(Random());
             } while(r < 0);

             u->setRadius(r);
             u->setAngle(2*M_PI*Random());
        }
 
        //Now sample radial angular angular coordinates for nodes in L2 dependently on their L1 coordinates.
        for (int i = 1; i <= N_2; i++) {

             u = new Node(i);
             G_2->addNewNode(u);

             Node *u_1 = G_1->findNode(i);

             if (u_1 == NULL) {

                 double r = 0;
                 do{
                    r = R_2 + 2*Beta_2*log(Random());
                 } while (r <0);
                 
                 u->setRadius(r);
                 u->setAngle(2*M_PI*Random());
             
             } else {
  
                 double r = 0;
                 double phi_1 = (R_1-u_1->getRadius())/(2*Beta_1);
                 do {
                     double tmp = exp(phi_1/(eta-1))*pow((Random()*pow(phi_1, 1-eta)), 1.0/(1-eta))/(eta-1);
                     const double w = gsl_sf_lambert_W0(tmp);
                     double phi_2 = pow(-pow(phi_1, eta)+pow((eta-1)*w, eta), 1.0/eta);
                     r = R_2 - 2*Beta_2*phi_2;
                 } while(r < 0);
                 
                 if(r > R_2) {
                   cout<<"Radial coordinate out of bounds\n";
                   exit(-1);
                 }
 
                 u->setRadius(r);
                
                 double theta_2 = get_correlated_angle_with(u_1->getAngle());
                 u->setAngle(theta_2);
            }
      }

      //Now construct the layers.
      //First construct L1.
      vector<Node*> nodes_1 = G_1->getNodeList();

      for (int i = 0; i < nodes_1.size(); i++) {
        
          //cout<<i<<" 1\n";

          for (int j = i+1; j < nodes_1.size(); j++) {

                u = nodes_1[i];
                v = nodes_1[j];

                double dtheta = fabs(v->getAngle()-u->getAngle());
                if(dtheta > M_PI) {
                   dtheta = 2*M_PI-dtheta;
                }

                double x = acosh((cosh(u->getRadius())*cosh(v->getRadius()))-(sinh(u->getRadius())*sinh(v->getRadius())*cos(dtheta)));
                double p = 1.0/(1+exp((1.0/(2*T_1))*(x-R_1)));

                if(Random() <= p) {
                    u->addAdjNode(v);
                    v->addAdjNode(u);    
                    out1 << v->getName() << " " << u->getName() <<"\n";
                }
         }

    }//End of L1 construction.

    out1.close();

    for (int i = 0; i < nodes_1.size(); i++) {
         u = nodes_1[i];
         out2 << u->getName() << " " << u->getAngle() << " " << u->getRadius()<<"\n";
    }
    out2.close();

    //Now construct L2.
    vector<Node*> nodes_2 = G_2->getNodeList();
       
    for (int i = 0; i < nodes_2.size(); i++) {
          
        //cout<<i<<" 2\n";

        for (int j = i+1; j < nodes_2.size(); j++) {

             u = nodes_2[i];
             v = nodes_2[j];

             double dtheta = fabs(v->getAngle()-u->getAngle());
             if(dtheta > M_PI) {
                dtheta = 2*M_PI-dtheta;
             }

             double x = acosh((cosh(u->getRadius())*cosh(v->getRadius()))-(sinh(u->getRadius())*sinh(v->getRadius())*cos(dtheta)));
             double p = 1.0/(1+exp((1.0/(2*T_2))*(x-R_2)));

             if(Random() <= p) {
                 u->addAdjNode(v);
                 v->addAdjNode(u);    
                 out3 << v->getName() << " " << u->getName() <<"\n";
             }
         }
    }//End of L2 construction.

    //Link persistence.
    //Sample w% links from layer 1 and connect with them the corresponding disconnected pairs in layer 2.
    for (int i = 0; i < nodes_1.size(); i++) {
       
        for (int j = i+1; j < nodes_1.size(); j++) {
             
              u = nodes_1[i];
              v = nodes_1[j];

              if(u->checkLink(v)) {

                  Node *u2 = G_2->findNode(u->getName());
                  Node *v2 = G_2->findNode(v->getName());
 
                  if ((u2 == NULL) || (v2 == NULL)) continue; //pair does not exist in L2.
               
                  if(!u2->checkLink(v2)) {//If u2 and v2 are disconnected in layer 2.
           
                     if(Random() <= w) {
                         u2->addAdjNode(v2);
                         v2->addAdjNode(u2);
                         out3 << v2->getName() << " " << u2->getName() <<"\n";
                     }          
                  }
              }
         }
    }    

    out3.close();

    for (int i = 0; i < nodes_2.size(); i++) {
         u = nodes_2[i];
         out4 << u->getName() << " " << u->getAngle() << " " << u->getRadius()<<" "<<u->getAdjNodeList().size()<<"\n";
    }
    out4.close();

}
