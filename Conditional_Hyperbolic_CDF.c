#include <math.h>
#include <cuba.h>
#include <iostream>
#include <fstream>

//Code for computing P(X_2 <= x_2 | r_1, r_1', dtheta_1).

double sigma, N_2, R_1, R_2, eta, g, beta_1, beta_2;

using namespace std;

static int Integrand (const int *ndim, const cubareal x[], const int *ncomp, cubareal f[], void *params) {

       //Hyperbolic distance in layer 2, x_2.
       double x_2 =  ((double*)params)[0];
      
       if((x_2  < 0) || (x_2 > 2*R_2)) {
           cout<<"Error: hyperbolic distance in layer 2 out of bounds, x_2="<<x_2<<"\n";
           exit(-1);
       }
    
       //tilde_x_2.
       double tilde_x_2 =  ((double*)params)[1];

       if((tilde_x_2  < 0) || (tilde_x_2 > 2*R_2)) {
           cout<<"Error: tilde_x_2 out of bounds, tilde_x_2="<<tilde_x_2<<"\n";
           exit(-1);
       }
       
       //Radial coordinates and angular distance in layer 1.
       double r_1 =  ((double*)params)[2];
       double r_2 =  ((double*)params)[3];
       double dtheta_1 =  ((double*)params)[4];

       if((r_1 < 0) || (r_2 < 0) || (r_1 > R_1) || (r_2 > R_1)) {
           cout<<"Error: radial coordinates in layer 1 out of bounds: r_1="<<r_1<<" r_2="<<r_2<<", while R_1="<<R_1<<"\n";
           exit(-1);
       }

       if((dtheta_1  < 0) || (dtheta_1 > M_PI)) {
           cout<<"Error: angular distance in layer 1 out of bounds: dtheta_1="<<dtheta_1<<"\n";
           exit(-1);
       }

       //Determine the radial coordinates in layer 2 and the integrand.
       //Notes on Integral rescaling: x[0], x[1] take values in [0,1] as Cuba performs multidimensional integration in the unit hypercube. We need to properly rescale their values to get corresponding radial coordinate values r_3, r_4. In each of the cases 1-16 below we have performed the rescaling using substitution as follows. If l_lower and l_upper are our actual lower and upper integration limits we solve the system of equations: lambda*l_upper+beta=1 and lambda*l_lower+beta=0 for lambda and beta, first for the outer integral (lambda_1, beta_1) and then for the inner integral (lambda_2, beta_2). Then, we have r_3=lambda_1*x[0]+beta_1, r_4=lambda_2*x[1]+beta_2, and dr_3=lambda_1*dx[0], dr_4=lambda_2*dx[1].
    
       double r_3, r_4, lambda_1, lambda_2;

       int integrand = -1.0;

       int integral =  ((double*)params)[5];

       switch(integral) {

           //Cases 1-5 correspond to the integrals in the paper for 0 <= tilde_x_2 <= R_2 (in the same order as in the paper).
           //Cases 6-11 correspond to the integrals in the paper for 0 <= x_2 <= R_2 <= tilde_x_2 (in the same order as in the paper).
           //Cases 12-16 correspond to the integrals in the paper for R_2 <= x_2 (in the same order as in the paper).

           case 1 :
             lambda_1 = tilde_x_2;
             r_3 = lambda_1*x[0];
             lambda_2 = r_3+R_2-tilde_x_2;
             r_4 = lambda_2*x[1]+tilde_x_2-r_3;
             integrand = 1;
             break;

           case 2 :
             lambda_1 = R_2-tilde_x_2;
             r_3 = lambda_1*x[0]+tilde_x_2;
             lambda_2 = R_2;
             r_4 = lambda_2*x[1];
             integrand = 1;
             break;

           case 3 :
             lambda_1 = x_2;
             r_3 = lambda_1*x[0];
             lambda_2 = tilde_x_2-x_2;
             r_4 = lambda_2*x[1]+x_2-r_3;
             integrand = 2;
             break;

           case 4 :
             lambda_1 = tilde_x_2-x_2;
             r_3 = lambda_1*x[0]+x_2;
             lambda_2 = tilde_x_2-r_3;
             r_4 = lambda_2*x[1];
             integrand = 2;
             break;

           case 5 :
             lambda_1 = x_2;
             r_3 = lambda_1*x[0];
             lambda_2 = x_2-r_3;
             r_4 = lambda_2*x[1];
             integrand = 0;
             break;

           case 6 :
             lambda_1 = 2*R_2-tilde_x_2;
             r_3 = lambda_1*x[0]+tilde_x_2-R_2;
             lambda_2 = r_3+R_2-tilde_x_2;
             r_4 = lambda_2*x[1]+tilde_x_2-r_3;
             integrand = 1;
             break;
       
           case 7 :
             lambda_1 = x_2;
             r_3 = lambda_1*x[0];
             lambda_2 = R_2-x_2;
             r_4 = lambda_2*x[1]+x_2-r_3;
             integrand = 2;
             break;

          case 8 :
             lambda_1 = R_2-x_2;
             r_3 = lambda_1*x[0]+x_2;
             lambda_2 = R_2-r_3;
             r_4 = lambda_2*x[1];
             integrand = 2;
             break;

           case 9 :
             lambda_1 = tilde_x_2-R_2;
             r_3 = lambda_1*x[0];
             lambda_2 = r_3;
             r_4 = lambda_2*x[1]+R_2-r_3;
             integrand = 2;
             break;

           case 10 :
             lambda_1 = 2*R_2-tilde_x_2;
             r_3 = lambda_1*x[0]+tilde_x_2-R_2;
             lambda_2 = tilde_x_2-R_2;
             r_4 = lambda_2*x[1]+R_2-r_3;
             integrand = 2;
             break;

           case 11 :
             lambda_1 = x_2;
             r_3 = lambda_1*x[0];
             lambda_2 = x_2-r_3;
             r_4 = lambda_2*x[1];
             integrand = 0;
             break;

           case 12 :
             lambda_1 = 2*R_2-tilde_x_2;
             r_3 = lambda_1*x[0]+tilde_x_2-R_2;
             lambda_2 = r_3+R_2-tilde_x_2;
             r_4 = lambda_2*x[1]+tilde_x_2-r_3;
             integrand = 1;
             break;

           case 13 :
             lambda_1 = tilde_x_2-x_2;
             r_3 = lambda_1*x[0]+x_2-R_2;
             lambda_2 = r_3+R_2-x_2;
             r_4 = lambda_2*x[1]+x_2-r_3;
             integrand = 2;
             break;

           case 14 :
             lambda_1 = 2*R_2-tilde_x_2;
             r_3 = lambda_1*x[0]+tilde_x_2-R_2;
             lambda_2 = tilde_x_2-x_2;
             r_4 = lambda_2*x[1]+x_2-r_3;
             integrand = 2;
             break;

           case 15 :
             lambda_1 = x_2-R_2;
             r_3 = lambda_1*x[0];
             lambda_2 = R_2;
             r_4 = lambda_2*x[1];
             integrand = 0;
             break;

           case 16 :
             lambda_1 = 2*R_2-x_2;
             r_3 = lambda_1*x[0]+x_2-R_2;
             lambda_2 = x_2-r_3;
             r_4 = lambda_2*x[1];
             integrand = 0;
             break;

           default :
             cout << "Invalid integral case" << endl;
             exit(-1);
       }

       if(integrand == -1) {
          cout<<"Error: unspecified integrand\n";
          exit(-1);
       }

       if((r_3 < 0) || (r_4 < 0) || (r_3 > R_2) || (r_4 > R_2)) {
          cout<<"Error: radial coordinates in layer 2 out of bounds: r_3="<<r_3<<" r_4="<<r_4<<", while R_2="<<R_2<<" (happened for integral "<<integral<<").\n";
          exit(-1);
       }

       double dtheta_2 = 2*asin(exp(0.5*(x_2-r_3-r_4)));

       if((dtheta_2 < 0) || (dtheta_2 > M_PI) || ((isnan(dtheta_2)) && (integrand!=0))) {
          cout<<"Error: angular distance in layer 2 out of bounds: dtheta_2="<<dtheta_2<<" (happened for integral "<<integral<<").\n";
          exit(-1);
       }

       //Phi's in layer 1.
       double phi1 = (R_1-r_1)/(2*beta_1);
       double phi2 = (R_1-r_2)/(2*beta_1);

       //Phi's in layer 2.
       double phi3 = (R_2-r_3)/(2*beta_2);
       double phi4 = (R_2-r_4)/(2*beta_2);

       //Conditional PDF of r_3 given r_1.
       double rho31 = exp(phi1-pow(pow(phi1,eta)+pow(phi3,eta),1.0/eta))*pow(phi1*phi3,eta-1)*pow(pow(phi1,eta)+pow(phi3,eta),1.0/eta-2)*(pow(pow(phi1,eta)+pow(phi3,eta),1.0/eta)+eta-1)/(2*beta_2);

       //Conditional PDF of r_4 given r_2.
       double rho42 = exp(phi2-pow(pow(phi2,eta)+pow(phi4,eta),1.0/eta))*pow(phi2*phi4,eta-1)*pow(pow(phi2,eta)+pow(phi4,eta),1.0/eta-2)*(pow(pow(phi2,eta)+pow(phi4,eta),1.0/eta)+eta-1)/(2*beta_2);
       
       if((rho31 < 0) || (rho42 < 0) || (!isfinite(rho31)) || (!isfinite(rho42)) || (isnan(rho31)) || (isnan(rho42))) {
          cout<<"Error: problem with conditional densities: "<<rho31<<", "<<rho42<<"\n";
          exit(-1);
       }

       double my_function = 0;

       if(integrand == 0) {

          my_function = 1.0;

       } else {
 
          if(integrand == 1) { //P1.

               my_function = (erf(N_2*(dtheta_2-dtheta_1)/(4*M_PI*sigma))+erf(N_2*(dtheta_2+dtheta_1)/(4*M_PI*sigma)))/(2*erf(N_2/(4*sigma)));
          
          } else {
 
              if(integrand == 2) { //P2.
            
                  my_function = 1-erf(N_2*(2*M_PI-dtheta_2-dtheta_1)/(4*M_PI*sigma))/(2*erf(N_2/(4*sigma)))+erf(N_2*(dtheta_2-dtheta_1)/(4*M_PI*sigma))/(2*erf(N_2/(4*sigma)));

             }
          }
      }
 
      if((my_function < 0) || (my_function > 1) || (isnan(my_function)) || (!isfinite(my_function))) {
          cout<<"Error with integrand function: integrand="<<my_function<<", for integral: "<<integral<<"\n";
          cout<<N_2<<" "<<dtheta_2<<" "<<dtheta_1<<" "<<sigma<<"\n";
          exit(-1);
      }
    
      //Also multiply with lambda_1, lambda_2 as dr_3=lambda_1*dx[0], dr_4=lambda_2*dx[1].
      f[0] = lambda_1*lambda_2*my_function*rho31*rho42;
       
      return 0;
}

/*********************************************************************/
#define NDIM 2

#define NCOMP 1
#define NVEC 1
#define EPSREL 1e-10
#define EPSABS 1e-12

#define VERBOSE 0
#define SEED 0
#define MINEVAL 0
//Increase MAXEVAL if there is a need for increasing accuracy. However, this will slow down the evaluation.
#define MAXEVAL 50000
#define NSTART 1000
#define NINCREASE 500
#define NBATCH 1000

int main(int argc, char *argv[]) {

  int neval, fail;
  cubareal integral[NCOMP], error[NCOMP], prob[NCOMP];

  if(argc != 14) {
     cout<<"Error: Usage ./a.out N_1 N_2 gamma_1 gamma_2 T_1 T_2 bar_k_1 bar_k_2 nu g r_1 r_2 dtheta_1\n";
     exit(-1);
  }

  double N_1 = strtold(argv[1], NULL);
  N_2 = strtold(argv[2], NULL);

  double gma1 = strtold(argv[3], NULL);
  double gma2 = strtold(argv[4], NULL);

  beta_1 = 1.0/(gma1-1);
  beta_2 = 1.0/(gma2-1);

  double T_1 = strtold(argv[5], NULL);
  double T_2 = strtold(argv[6], NULL);

  double bar_k_1 = strtold(argv[7], NULL);
  double bar_k_2 = strtold(argv[8], NULL);

  double c_1 = bar_k_1*(sin(T_1*M_PI)/(2*T_1))*pow((gma1-2)/(gma1-1),2);
  double c_2 = bar_k_2*(sin(T_2*M_PI)/(2*T_2))*pow((gma2-2)/(gma2-1),2);

  R_1 = 2*log(N_1/c_1);
  R_2 = 2*log(N_2/c_2);

  double nu = strtold(argv[9], NULL);
  
  if ((nu < 0) || (nu >= 1)) {
     cout<<"Please give a nu in [0, 1).\n";
     exit(-1);
  }

  eta = 1.0/(1.0-nu);

  double g = strtold(argv[10], NULL);
 
  if((g <= 0) || (g > 1)) {
     cout<<"Please give a g in (0, 1].\n";
     exit(-1);
  }

  double sigma0 = N_2/(4*M_PI);
  if(sigma0 > 100) sigma0 = 100;
  sigma = sigma0*(1.0/g-1);

  double r_1 = strtold(argv[11], NULL);
  double r_2 = strtold(argv[12], NULL);
  double dtheta_1 = strtold(argv[13], NULL);

  double bins = 0.1;

  cout<<"\n";
  cout<<"Estimating conditional CDF (uses bins of size "<<bins<<") for:\n";
  cout<<"dtheta_1="<<dtheta_1<<", r_1="<<r_1<<", r_2="<<r_2<<",\n";
  cout<<"nu="<<nu<<", g="<<g<<",\n";
  cout<<"R_1="<<R_1<<", R_2="<<R_2<<",\n"; 
  cout<<"N_1="<<N_1<<", N_2="<<N_2<<",\n"; 
  cout<<"gamma1="<<gma1<<", gamma2="<<gma2<<",\n";
  cout<<"T_1="<<T_1<<", T_2="<<T_2<<",\n"; 
  cout<<"bark_1="<<bar_k_1<<", bar_k_2="<<bar_k_2<<".\n";
  cout<<"\n";
    
  //Output the CDF.
  ofstream out1;
  out1.precision(15);
  out1.open("cond_hyperbolic_cdf.txt");

  double start_val = 0;
  
  double x_2 = start_val;

  while(x_2 <= 2*R_2) {
  
       double tilde_x_2 = x_2-2*log(sin((M_PI-dtheta_1)/2));
       if(tilde_x_2 > 2*R_2) tilde_x_2 = 2*R_2;
 
       if((tilde_x_2 < 0) || (x_2 > tilde_x_2)) {
          cout<<"Error tilde_x_2: "<<tilde_x_2<<"\n";
          exit(-1);
       }

       //Region 1 (I_1):
       if(tilde_x_2 <= R_2){

              double params1[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 1};
              double params2[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 2};
              double params3[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 3};
              double params4[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 4};
              double params5[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 5};

              Vegas(NDIM, NCOMP, Integrand, params1, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              double I_1 = (double)integral[0];
        
              Vegas(NDIM, NCOMP, Integrand, params2, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_1 = I_1 + (double)integral[0];
              
              Vegas(NDIM, NCOMP, Integrand, params3, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_1 = I_1 + (double)integral[0];

              Vegas(NDIM, NCOMP, Integrand, params4, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_1 = I_1 + (double)integral[0];

              Vegas(NDIM, NCOMP, Integrand, params5, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_1 = I_1 + (double)integral[0];

              out1<<x_2<<" "<<I_1<<"\n";
       }

       //Region 2 (I_2):
       if ((x_2 <= R_2) && (tilde_x_2 > R_2)) {

              double params1[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 6};
              double params2[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 7};
              double params3[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 8};
              double params4[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 9};
              double params5[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 10};
              double params6[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 11};
              
              Vegas(NDIM, NCOMP, Integrand, params1, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              double I_2 = (double)integral[0];
              
              Vegas(NDIM, NCOMP, Integrand, params2, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_2 = I_2 + (double)integral[0];
              
              Vegas(NDIM, NCOMP, Integrand, params3, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_2 = I_2 + (double)integral[0];
             
              Vegas(NDIM, NCOMP, Integrand, params4, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_2 = I_2 + (double)integral[0];
              
              Vegas(NDIM, NCOMP, Integrand, params5, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_2 = I_2 + (double)integral[0];
              
              Vegas(NDIM, NCOMP, Integrand, params6, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_2 = I_2 + (double)integral[0];
              
              out1<<x_2<<" "<<I_2<<"\n";

       }

       //Region 3 (I_3):
       if (x_2 > R_2) {

              double params1[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 12};
              double params2[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 13};
              double params3[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 14};
              double params4[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 15};
              double params5[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 16};
              
              Vegas(NDIM, NCOMP, Integrand, params1, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              double I_3 = (double)integral[0];
              
              Vegas(NDIM, NCOMP, Integrand, params2, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_3 = I_3 + (double)integral[0];
              
              Vegas(NDIM, NCOMP, Integrand, params3, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_3 = I_3 + (double)integral[0];
              
              Vegas(NDIM, NCOMP, Integrand, params4, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_3 = I_3 + (double)integral[0];

              Vegas(NDIM, NCOMP, Integrand, params5, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral, error, prob);
              I_3 = I_3 + (double)integral[0];

              out1<<x_2<<" "<<I_3<<"\n";
       }
       
       x_2 = x_2 + bins;
  }

  out1.close();

  return 0;
}

