
This folder contains the following programs from the paper: "Link persistence and conditional distances in multiplex networks", F. Papadopoulos, K.-K. Kleineberg, Physical Review E, Vol. 99, Iss. 1, 2019 (https://arxiv.org/abs/1807.01190).


1) GMM-LP.cpp
-------------
 This C++ program implements the Geometric Multiplex Model with Link Persistence, described in Section IV of the paper.

- The parameters of the model can be tuned inside the code (see the comments in the code). Alternatively, you may want to modify the code to take these parameters as input from the user.

- It requires the GNU Scientific Library (GSL).

- To compile: g++ GMM-LP.cpp -lgsl

- To run: ./a.out

- The program produces 4 output files: l_1.txt, l_2.txt, l_1_coordinates.txt, l_2_coordinates.txt.

l_i.txt contains layer i, while l_i_coordinates.txt contains the hyperbolic node coordinates in layer i, i=1,2. 

Each line in l_i.txt is of the form: 

id_1 id_2 

if node with id_1 is connected with node with id_2. 

Each line in l_i_coordinates.txt is of the form: 

id angular_coordinate radial_coordinate

where id is the node id, angular_coordinate is the node's angular coordinate (in [0, 2*pi]) and radial_coordinate is the node's radial coordinate.


2) Conditional_Angular_CDF.pl 
-----------------------------
This is a perl script computing the conditional angular CDF derived in Appendix D.A of the paper.

- The parameters g (angular correlation strength) and N_2 (number of nodes in layer 2) can be tuned inside the code (see comments in the code). Parameter dtheta_1 (angular distance in layer 1) is passed as input. 

The perl modules Math::Gauss, Math::Prime::Util may need to be installed.

For example, to run with dtheta_1 = 1.5 write: perl Conditional_Angular_CDF.pl 1.5 > c_ang.txt 

The CDF will be stored in c_ang.txt.


3) Conditional_Hyperbolic_CDF.c 
-------------------------------
This C/C++ program computes the conditional hyperbolic CDF P(X_2 <= x_2 | r_1, r_2, dtheta_1) derived in Appendix D.B of the paper.

- It requires the Cuba library (http://www.feynarts.de/cuba/) for multidimensional numerical integration.

- To compile: g++ Conditional_Hyperbolic_CDF.c -lcuba

- To run: ./a.out N_1 N_2 gamma_1 gamma_2 T_1 T_2 bar_k_1 bar_k_2 nu g r_1 r_2 dtheta_1 

where: 

N_i, gamma_i, T_i, bar_k_i are respectively the number of nodes, power-law degree distribution exponent, temperature, and average degree in layer i=1,2, 

nu, g are the radial and angular correlation strengths,

r_1, r_2, dtheta_1 are the nodes' radial coordinates and angular distance in layer 1.

- The output of the program (conditional hyperbolic CDF) is stored in cond_hyperbolic_cdf.txt. 

- The bin size can be tuned in line 335 of the code. More comments can be found inside the code.


4) Conditional_Hyperbolic_PDF.c. 
--------------------------------
Same as above but for the conditional hyperbolic PDF f(x_2 | r_1, r_2, dtheta_1). The code corresponds to the derivatives of Eqs. (59)-(61) in Appendix D.B of the paper.

- To compile: g++ Conditional_Hyperbolic_PDF.c -lcuba

- To run: ./a.out N_1 N_2 gamma_1 gamma_2 T_1 T_2 bar_k_1 bar_k_2 nu g r_1 r_2 dtheta_1 

where the input parameters are the same as above.

- The output of the program (histogram using bins of size 0.1) is stored in cond_hyperbolic_pdf.txt 

- The bin size can be tuned in line 371 of the code. More comments can be found inside the code.



Author: Fragkiskos Papadopoulos (email: f.papadopoulos@cut.ac.cy)
