#include <math.h>
#include <cuba.h>
#include <iostream>
#include <fstream>

//Code for computing the conditional PDF f(x_2 | r_1, r_1', dtheta_1).

double sigma, N_2, R_1, R_2, eta, g, beta_1, beta_2;

using namespace std;

double get_integrand2 (double r_1, double r_2, double r_3, double r_4, double x_2, double dtheta_1, double integrand) {

       if((r_1 < 0) || (r_2 < 0) || (r_1 > R_1) || (r_2 > R_1)) {
           cout<<"Error: radial coordinates in layer 1 out of bounds: r_1="<<r_1<<" r_2="<<r_2<<" while R_1="<<R_1<<" (happened for integrand "<<integrand<<").\n";
           exit(-1);
       }

       if((dtheta_1  < 0) || (dtheta_1 > M_PI)) {
           cout<<"Error: angular distance in layer 1 out of bounds: dtheta_1="<<dtheta_1<<" (happened for integrand "<<integrand<<").\n";
           exit(-1);
       }

        if((r_3 < 0) || (r_4 < 0) || (r_3 > R_2) || (r_4 > R_2)) {
           cout<<"Error: radial coordinates in layer 2 out of bounds: r_3="<<r_3<<" r_4="<<r_4<<" while R_2="<<R_2<<" (happened for integrand "<<integrand<<").\n";
           exit(-1);
        }

        double dtheta_2 = 2*asin(exp(0.5*(x_2-r_3-r_4)));

        double tmp = exp(0.5*(x_2-r_3-r_4));
    
        //Make sure we don't get errors when tmp = 1.
        if ((tmp > 1.0) && (tmp < 1.0 + pow(10, -15))) dtheta_2 = M_PI;

        if((dtheta_2 < 0) || (dtheta_2 > M_PI) || ((isnan(dtheta_2)) && (integrand != 0))) {
           cout<<"Error: angular distance in layer 2 out of bounds: dtheta_2="<<dtheta_2<<" (happened for integrand "<<integrand<<").\n";
           exit(-1);
        }

        //Phi's in layer 1.
        double phi1 = (R_1-r_1)/(2*beta_1);
        double phi2 = (R_1-r_2)/(2*beta_1);

        //Phi's in layer 2.
        double phi3 = (R_2-r_3)/(2*beta_2);
        double phi4 = (R_2-r_4)/(2*beta_2);

        //Conditional PDF of r_3 given r_1.
        double rho31 = exp(phi1-pow(pow(phi1,eta)+pow(phi3,eta),1.0/eta))*pow(phi1*phi3,eta-1)*pow(pow(phi1,eta)+pow(phi3,eta),1.0/eta-2)*(pow(pow(phi1,eta)+pow(phi3,eta),1.0/eta)+eta-1)/(2*beta_2);

        //Conditional PDF of r_4 given r_2.
        double rho42 = exp(phi2-pow(pow(phi2,eta)+pow(phi4,eta),1.0/eta))*pow(phi2*phi4,eta-1)*pow(pow(phi2,eta)+pow(phi4,eta),1.0/eta-2)*(pow(pow(phi2,eta)+pow(phi4,eta),1.0/eta)+eta-1)/(2*beta_2);

        if((rho31 < 0) || (rho42 < 0) || (!isfinite(rho31)) || (!isfinite(rho42)) || (isnan(rho31)) || (isnan(rho42))) {
           cout<<"Error: problem with conditional densities: "<<rho31<<", "<<rho42<<"\n";
           exit(-1);
        }

        double my_function = 0;

        if(integrand == 1) {
            
                 double tmp1 = exp(-pow(N_2*(dtheta_1-dtheta_2)/(4*M_PI*sigma),2));
                 double tmp2 = exp(-pow(N_2*(dtheta_1+dtheta_2)/(4*M_PI*sigma),2));
                 double tmp3 = exp(0.5*(x_2-r_3-r_4));
                 double tmp4 = 4.0*sqrt(1-exp(x_2-r_3-r_4))*pow(M_PI, 3.0/2.0)*sigma*erf(N_2/(4*sigma));
                 my_function = tmp3*(tmp1+tmp2)*N_2/tmp4;
                

        } else {

                if(dtheta_2 == M_PI){
                    
                      //If we come here it means that tmp4 = 0 in the denominator below.
                      //So we avoid the division by 0 by setting my_function = 0 here (i.e., by ignoring this event).
                      my_function = 0;
                    
                } else {
                    
                      double tmp1 = exp(-pow(N_2*(2*M_PI-dtheta_1-dtheta_2)/(4*M_PI*sigma),2));
                      double tmp2 = exp(-pow(N_2*(dtheta_2-dtheta_1)/(4*M_PI*sigma),2));
                      double tmp3 = exp(0.5*(x_2-r_3-r_4));
                      double tmp4 = 4.0*sqrt(1-exp(x_2-r_3-r_4))*pow(M_PI, 3.0/2.0)*sigma*erf(N_2/(4*sigma));
                      my_function = tmp3*(tmp1+tmp2)*N_2/tmp4;
                }
        }

        if((my_function < 0) || (isnan(my_function)) || (!isfinite(my_function))) {
            cout<<"Error with integrand function: integrand="<<my_function<<", for integrand: "<<integrand<<"\n";
            cout<<dtheta_2<<" "<<dtheta_1<<" "<<x_2-r_3-r_4<<"\n";
            exit(-1);
        }

        return my_function*rho31*rho42;
}

static int Integrand2 (const int *ndim, const cubareal x[], const int *ncomp, cubareal f[], void *params) {

       double x_2 =  ((double*)params)[0];
       double tilde_x_2 =  ((double*)params)[1];
       double r_1 =  ((double*)params)[2];
       double r_2 =  ((double*)params)[3];
       double dtheta_1 =  ((double*)params)[4];
       double which_case =  ((double*)params)[5];

       double epsilon = x_2-tilde_x_2;

       if (epsilon > 0 ) {
           cout<<"Error: epsilon should be less than 0...\n";
           exit(-1);
       }

       //Determine the radial coordinates in layer 2 and the integrand.
       //Notes on Integral rescaling: x[0], x[1] take values in [0,1] as Cuba performs multidimensional integration in the unit hypercube. We need to properly rescale their values to get corresponding radial coordinate values r_3, r_4. In each of the cases below we have performed the rescaling using substitution as follows. If l_lower and l_upper are our actual lower and upper integration limits we solve the system of equations: lambda*l_upper+beta=1 and lambda*l_lower+beta=0 for lambda and beta, first for the outer integral (lambda_1, beta_1) and then for the inner integral (lambda_2, beta_2). Then, we have r_4=lambda_1*x[0]+beta_1, r_3=lambda_2*x[1]+beta_2, and dr_4=lambda_1*dx[0], dr_3=lambda_2*dx[1].
    
       double my_integral = 0;

       if(which_case == 1) { //Region: 0 <= tilde_x_2 <= R_2.

           //The derivatives below are from Integrals 1-4 for this region, in the same order as in the paper.
           //Other derivative terms in this region cancel out. 

           //From derivative of Integral 1:
           double upper_limit_1 = x_2-epsilon;
           double lower_limit_1 = 0;
           double lambda_1 = upper_limit_1-lower_limit_1;
           double mu = lower_limit_1;
           double r_4 = lambda_1*x[0]+mu;
           double upper_limit_2 = R_2;
           double lower_limit_2 = x_2-epsilon-r_4;
           double lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           double r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 1);

           //From derivative of Integral 2:
           upper_limit_1 = R_2;
           lower_limit_1 = x_2-epsilon;
           lambda_1 = upper_limit_1-lower_limit_1;
           mu = lower_limit_1;
           r_4 = lambda_1*x[0]+mu;
           upper_limit_2 = R_2;
           lower_limit_2 = 0;
           lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 1);

           //From derivative of Integral 3:
           upper_limit_1 = x_2;
           lower_limit_1 = 0;
           lambda_1 = upper_limit_1-lower_limit_1;
           mu = lower_limit_1;
           r_4 = lambda_1*x[0]+mu;
           upper_limit_2 = x_2-epsilon-r_4;
           lower_limit_2 = x_2-r_4;
           lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 2);

           //From derivative of Integral 4:
           upper_limit_1 = x_2-epsilon;
           lower_limit_1 = x_2;
           lambda_1 = upper_limit_1-lower_limit_1;
           mu = lower_limit_1;
           r_4 = lambda_1*x[0]+mu;
           upper_limit_2 = x_2-epsilon-r_4;
           lower_limit_2 = 0;
           lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 2);

       }
 
       if(which_case == 2) { //Region: x_2 <= R_2 < tilde_x_2.

           //The derivatives below are from Integrals 1-5 for this region, in the same order as in the paper.
           //Other derivative terms in this region cancel out. 

           //From derivative of Integral 1:
           double upper_limit_1 = R_2;
           double lower_limit_1 = x_2-epsilon-R_2;
           double lambda_1 = upper_limit_1-lower_limit_1;
           double mu = lower_limit_1;
           double r_4 = lambda_1*x[0]+mu;
           double upper_limit_2 = R_2;
           double lower_limit_2 = x_2-epsilon-r_4;
           double lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           double r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 1);

           //From derivative of Integral 2:
           upper_limit_1 = x_2;
           lower_limit_1 = 0;
           lambda_1 = upper_limit_1-lower_limit_1;
           mu = lower_limit_1;
           r_4 = lambda_1*x[0]+mu;
           upper_limit_2 = R_2-r_4;
           lower_limit_2 = x_2-r_4;
           lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 2);

           //From derivative of Integral 3:
           upper_limit_1 = R_2;
           lower_limit_1 = x_2;
           lambda_1 = upper_limit_1-lower_limit_1;
           mu = lower_limit_1;
           r_4 = lambda_1*x[0]+mu;
           upper_limit_2 = R_2-r_4;
           lower_limit_2 = 0;
           lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 2);
          
           //From derivative of Integral 4:
           upper_limit_1 = x_2-R_2-epsilon;
           lower_limit_1 = 0;
           lambda_1 = upper_limit_1-lower_limit_1;
           mu = lower_limit_1;
           r_4 = lambda_1*x[0]+mu;
           upper_limit_2 = R_2;
           lower_limit_2 = R_2-r_4;
           lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 2);
       
           //From derivative of Integral 5:
           upper_limit_1 = R_2;
           lower_limit_1 = x_2-epsilon-R_2;
           lambda_1 = upper_limit_1-lower_limit_1;
           mu = lower_limit_1;
           r_4 = lambda_1*x[0]+mu;
           upper_limit_2 = x_2-epsilon-r_4;
           lower_limit_2 = R_2-r_4;
           lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 2);
       
       }

       if(which_case == 3) { //Region: R_2 < x_2.

           //The derivatives below are from Integrals 1-3 for this region, in the same order as in the paper.
           //Other derivative terms in this region cancel out. 

           //From derivative of Integral 1:
           double upper_limit_1 = R_2;
           double lower_limit_1 = x_2-epsilon-R_2;
           double lambda_1 = upper_limit_1-lower_limit_1;
           double mu = lower_limit_1;
           double r_4 = lambda_1*x[0]+mu;
           double upper_limit_2 = R_2;
           double lower_limit_2 = x_2-epsilon-r_4;
           double lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           double r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 1);

           //From derivative of Integral 2:
           upper_limit_1 = x_2-epsilon-R_2;
           lower_limit_1 =  x_2-R_2;
           lambda_1 = upper_limit_1-lower_limit_1;
           mu = lower_limit_1;
           r_4 = lambda_1*x[0]+mu;
           upper_limit_2 = R_2;
           lower_limit_2 = x_2-r_4;
           lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 2);

           //From derivative of Integral 3:
           upper_limit_1 = R_2;
           lower_limit_1 = x_2-epsilon-R_2;
           lambda_1 = upper_limit_1-lower_limit_1;
           mu = lower_limit_1;
           r_4 = lambda_1*x[0]+mu;
           upper_limit_2 = x_2-r_4-epsilon;
           lower_limit_2 =  x_2-r_4;
           lambda_2 = upper_limit_2-lower_limit_2;
           mu = lower_limit_2;
           r_3 = lambda_2*x[1]+mu;
           my_integral = my_integral+lambda_1*lambda_2*get_integrand2(r_1, r_2, r_3, r_4, x_2, dtheta_1, 2);
       
       }

       f[0] = my_integral;

       return 0;

}

/*********************************************************************/
#define NVEC 1
#define EPSREL 1e-10
#define EPSABS 1e-12

#define VERBOSE 0
#define SEED 0
#define MINEVAL 0
//Increase MAXEVAL if there is a need for increasing accuracy. However, this will slow down the evaluation.
#define MAXEVAL 100000
#define NSTART 1000
#define NINCREASE 500
#define NBATCH 1000

int main(int argc, char *argv[]) {

  int neval, fail;
  cubareal integral[1], integral1[1], error[1], prob[1];

  if(argc != 14) {
     cout<<"Error: Usage ./a.out N_1 N_2 gamma_1 gamma_2 T_1 T_2 bar_k_1 bar_k_2 nu g r_1 r_2 dtheta_1\n";
     exit(-1);
  }

  double N_1 = strtold(argv[1], NULL);
  N_2 = strtold(argv[2], NULL);

  double gma1 = strtold(argv[3], NULL);
  double gma2 = strtold(argv[4], NULL);

  beta_1 = 1.0/(gma1-1);
  beta_2 = 1.0/(gma2-1);

  double T_1 = strtold(argv[5], NULL);
  double T_2 = strtold(argv[6], NULL);

  double bar_k_1 = strtold(argv[7], NULL);
  double bar_k_2 = strtold(argv[8], NULL);

  double c_1 = bar_k_1*(sin(T_1*M_PI)/(2*T_1))*pow((gma1-2)/(gma1-1),2);
  double c_2 = bar_k_2*(sin(T_2*M_PI)/(2*T_2))*pow((gma2-2)/(gma2-1),2);

  R_1 = 2*log(N_1/c_1);
  R_2 = 2*log(N_2/c_2);

  double nu = strtold(argv[9], NULL);
  
  if ((nu < 0) || (nu >= 1)) {
     cout<<"Please give a nu in [0, 1).\n";
     exit(-1);
  }

  eta=1.0/(1.0-nu);

  double g = strtold(argv[10], NULL);
 
  if((g <= 0) || (g >= 1)) {
     cout<<"Please give a g in (0, 1).\n";
     exit(-1);
  }

  double sigma0 = N_2/(4*M_PI);
  if(sigma0 > 100) sigma0 = 100;
  sigma = sigma0*(1.0/g-1);

  double r_1 = strtold(argv[11], NULL);
  double r_2 = strtold(argv[12], NULL);
  double dtheta_1 = strtold(argv[13], NULL);

  double bins = 0.1;

  cout<<"\n";
  cout<<"Estimating conditional PDF (uses bins of size "<<bins<<") for:\n";
  cout<<"dtheta_1="<<dtheta_1<<", r_1="<<r_1<<", r_2="<<r_2<<",\n";
  cout<<"nu="<<nu<<", g="<<g<<",\n";
  cout<<"R_1="<<R_1<<", R_2="<<R_2<<",\n"; 
  cout<<"N_1="<<N_1<<", N_2="<<N_2<<",\n"; 
  cout<<"gamma1="<<gma1<<", gamma2="<<gma2<<",\n";
  cout<<"T_1="<<T_1<<", T_2="<<T_2<<",\n"; 
  cout<<"bark_1="<<bar_k_1<<", bar_k_2="<<bar_k_2<<".\n";
  cout<<"\n";

  //Output the conditional PDF.
  ofstream out1;
  out1.precision(15);
  out1.open("cond_hyperbolic_pdf.txt");

  double start_val = 0;
  double x_2 = start_val;

  //Phi's in layer 1.
  double phi1 = (R_1-r_1)/(2*beta_1);
  double phi2 = (R_1-r_2)/(2*beta_1);

  while(x_2 <= 2*R_2) {
  
       double tilde_x_2 = x_2-2*log(sin((M_PI-dtheta_1)/2));
       if(tilde_x_2 > 2*R_2) tilde_x_2 = 2*R_2;
 
       if((tilde_x_2 < 0) || (x_2 > tilde_x_2)) {
          cout<<"Error tilde_x_2: "<<tilde_x_2<<"\n";
          exit(-1);
       }

       //Region 1:
       if(tilde_x_2 <= R_2){

            double params[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 1};

            Vegas(2, 1, Integrand2, params, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral1, error, prob);
            double pdf = integral1[0];

            out1<<x_2<<" "<<pdf*bins<<"\n";
      }

      //Region 2:
      if ((x_2 <= R_2) && (tilde_x_2 > R_2)) {

            double params[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 2};

            Vegas(2, 1, Integrand2, params, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral1, error, prob);
            double pdf = integral1[0];

            out1<<x_2<<" "<<pdf*bins<<"\n";
       }

       //Region 3:
       if (x_2 > R_2) { 

           double params[6] = {x_2, tilde_x_2, r_1, r_2, dtheta_1, 3};
              
           Vegas(2, 1, Integrand2, params, NVEC, EPSREL, EPSABS, VERBOSE, SEED, MINEVAL, MAXEVAL, NSTART, NINCREASE, NBATCH, 0, NULL, NULL, &neval, &fail, integral1, error, prob);
           double pdf = integral1[0];

           out1<<x_2<<" "<<pdf*bins<<"\n";

       }
       
       x_2 = x_2 + bins;
  }

  out1.close();

  return 0;
}

