#!/usr/bin/perl
#Code that computes the conditional CDF P(dtheta_2 < dtheta | dtheta_1).
#dtheta_1 is passed as input (line 10) while g and N_2 can be tuned in lines 11,12.

use strict;
use Math::Gauss ':all';

my $pi=3.141592653;

my $dtheta_1 = $ARGV[0];
my $g = 0.5;
my $N_2 = 3000;

my $sigma0 = $N_2/(4*$pi);
if ($sigma0 > 100) {
    $sigma0 = 100;
}
my $sigma = $sigma0*(1.0/$g-1);

for(my $dtheta = 0.0; $dtheta <= $pi; $dtheta = $dtheta + 0.01){

    my $cdf = -1;
  
    if($dtheta <= $pi-$dtheta_1) {

        $cdf = (erf($N_2*($dtheta-$dtheta_1)/(4*$pi*$sigma))+erf($N_2*($dtheta+$dtheta_1)/(4*$pi*$sigma)))/(2*erf($N_2/(4*$sigma)));  
   
    } else {

        $cdf = 1-erf($N_2*(2*$pi-$dtheta-$dtheta_1)/(4*$pi*$sigma))/(2*erf($N_2/(4*$sigma)))+erf($N_2*($dtheta-$dtheta_1)/(4*$pi*$sigma))/(2*erf($N_2/(4*$sigma)));  

    }

    printf "$dtheta $cdf\n";
}

sub erf($) {

   my ($x) = @_;
   return 2*cdf(sqrt(2)*$x)-1;
}
